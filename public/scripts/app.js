'use strict';

angular.module('youmightnotneedieApp', [
  'ngSanitize'
]);

$(function(){
  $('body').on('click', 'h1', function(){
    $('.ie').animo({
      animation: 'hinge',
      duration: 3
    });
  });
});